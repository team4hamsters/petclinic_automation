pipeline {
  agent any
  options {
    timeout(time: 1, unit: 'HOURS')
    disableConcurrentBuilds()
    timestamps()
  }
  triggers{
    bitbucketpr(projectPath:'https://bitbucket.org/team4hamsters/petclinic_automation',
    cron:'H/15 * * * *',
    credentialsId:'bitbucket_vd_credentials',
    username:'',
    password:'',
    repositoryOwner:'team4hamsters',
    repositoryName:'petclinic_automation',
    branchesFilter:'',
    branchesFilterBySCMIncludes:false,
    ciKey:'PCAServer',
    ciName:'PCAutomation',
    ciSkipPhrases:'',
    checkDestinationCommit:false,
    approveIfSuccess:true,
    cancelOutdatedJobs:true,
    commentTrigger:'This should work, probably ```\\_(o_o)_/```')
  }
  stages {
    stage('Validation') {
      options {
        timeout(time: 20, unit: 'MINUTES')
      }
      steps {
        script {
          def date = new Date()
          currentBuild.displayName=date.format("yyyy-MM-dd_H")
        }
        sh 'mvn -N io.takari:maven:wrapper'
        sh './mvnw validate'
      }
    }
    stage('Build') {
      steps {
        sh './mvnw clean compile -Dmaven.test.skip=true'
      }
    }
    stage('Test') {
      steps {
        sh './mvnw test'
      }
      post {
        always {
          sh 'echo "Gathering test results"'
          junit '**/target/surefire-reports/*.xml'
        }
        aborted {
          echo 'Test stage has been aborted. Perhaps, because time ends up.'
        }
        failure {
          echo 'Something goes wrong.'
        }
      }
    }
    stage('Packaging') {
      steps {
        sh './mvnw package -Dmaven.test.skip=true'
      }
    }
    stage('Archive and upload to s3') {
      environment {
        JAR_NAME=sh(returnStdout: true, script: 'echo -n $(find . -regextype "egrep" -regex ".*petclinic.*\\.jar$")')
        ARCHIVE_NAME=sh(returnStdout: true, script: 'echo -n ${GIT_BRANCH#*/}_$(date +"%F_%H-%M").zip')
      }
      steps {
        sh 'echo "web: java -jar ${JAR_NAME##*/} --server.port=8090" > Procfile'
        sh 'zip -j ${ARCHIVE_NAME} ${JAR_NAME} Procfile'
        sh 'zip ${ARCHIVE_NAME} .ebextensions/loadbalancer-terminatehttps.config'
        s3Upload consoleLogLevel: 'INFO',
        dontWaitForConcurrentBuildCompletion: false,
        entries: [[bucket: 'petclinic-bucket-artifacts/petclinic_artifacts',
        excludedFile: '', flatten: true, gzipFiles: false, keepForever: false,
        managedArtifacts: false, noUploadOnFailure: true,
        selectedRegion: 'us-west-1', showDirectlyInBrowser: true,
        sourceFile: '*.zip', storageClass: 'STANDARD',
        uploadFromSlave: false, useServerSideEncryption: false]],
        pluginFailureResultConstraint: 'FAILURE',
        profileName: 'uploader', userMetadata: []
      }
    }
    stage('Deploy') {
      when {
        anyOf {
          branch 'release'
          branch 'master'
        }
      }
      steps {
        sh 'echo "Deploying of the application from branch ${GIT_BRANCH#*/} starts here."'
      }
    }
  }
}
