#!/bin/bash

docker container stop PetClinicApplication
docker container rm PetClinicApplication
docker image rm petclinicapplication_image
