#!/bin/bash

ERROR=7

javaJDK=${1:-""}

echo "Start Java Oracle JDK installing."
cd /opt/;

if [[ ! -f $javaJDK ]]; then
  echo "Java JDK was not found."
  exit $ERROR;
fi

JDK_NAME=$(tar -tzf /opt/$javaJDK | head -1 | cut --delimiter='/' --field=1)

tar -xzf /opt/$javaJDK -C /opt/;

echo "export JAVA_HOME=/opt/$JDK_NAME" >> ~/.bashrc; 
echo "export PATH=/opt/$JDK_NAME/bin:$PATH" >> ~/.bashrc;
echo "export JRE_HOME=/opt/$JDK_NAME/jre" >> ~/.bashrc;
rm /opt/$javaJDK;

#export JAVA_HOME=/opt/$JDK_NAME;
#export PATH=/opt/$JDK_NAME/bin:$PATH
#export JRE_HOME=/opt/$JDK_NAME/jre

exit 0;
