#!/bin/bash

ERROR=10

if [ -z "$1" ]; then
  echo "Usage `basename $0` imageName:version"
  exit $ERROR
fi

docker run -it -p 8080:8080 --name PetClinicApplication $1
